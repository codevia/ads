const crypto = require('crypto');
const iv = "fedcba9876543210";

module.exports = {
 AESKeyVerifier : (key, pad) => {
        if(pad == null) pad = " ";
        let keyLength = key.length;
        let factor = parseInt(Math.ceil(parseFloat(keyLength) / 16))
        if (factor == 0) factor = 1;

        for (let i = keyLength; i < factor * 16; i++) {
            key += pad;
        }
        return key;
    },

encrypt : (key, data) => {
        let cipher  = crypto.createCipheriv('aes-128-cbc', key, iv);
        let crypted = cipher.update(data, 'utf8', 'binary');

        crypted += cipher.final('binary');
        crypted = Buffer.from(crypted, 'binary').toString('hex')

        return crypted;
    }
}
