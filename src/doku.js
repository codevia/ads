const crypto   = require('crypto');
const fetch    = require('node-fetch');
const { DateTime } = require('luxon');
const randomstring = require("randomstring");
const hash         = require('./lib/AES128')

class Doku {
    constructor(options) {
        this.options = {
            url         : options.url,
            key         : options.key,
            merchantid  : options.merchantid
        }
    }

    /**
     * Method for generate the WORDS for encryption DOKU
     * @param {string} data 
     * @param {Boolean} auth 
     */
    getWords(data, auth){
        let createWords = `${data.CHANNELCODE}${data.REQUESTDATETIME}${this.options.key}${data.LOGINNAME}`;
        if (auth) createWords = `${data.CHANNELCODE}${data.SESSIONID}${data.REQUESTDATETIME}${this.options.key}`;
        return crypto.createHash('sha1').update(createWords).digest('hex');
    }

    /**
     * Check network doku is available
     */
    NetworkManagement() {
        return fetch(this.options.url + 'NetworkManagement?', {
            method: 'POST'
        })
        .then(response => response.json())
    }

    /**
     * Authentication DOKU API
     * @param {Object} params 
     */
    login(params) {
        if(Object.keys(params).length < 1) throw { responsemsg: "The argument menthod canot be empty" }

        if(!params.REQUESTDATETIME) params.REQUESTDATETIME = DateTime.local().toFormat('yyyyLLddHHmmss');

        const key = hash.AESKeyVerifier(this.options.key, 0);
        if(!params.WORDS) params.WORDS = this.getWords(params);
        
        if(params.PASSWORD) params.PASSWORD = hash.encrypt(key, params.PASSWORD);

        const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&');
        return this.post('AgentLoginMIP', query);
    }
    
    /**
     * Method for topup balance
     * @param {Object} params 
     */
    TopUpInit(params){
        if(Object.keys(params).length < 1) throw { responsemsg: "The argument menthod canot be empty" };

        if(!params.REQUESTDATETIME) params.REQUESTDATETIME = DateTime.local().toFormat('yyyyLLddHHmmss');

        const key = hash.AESKeyVerifier(this.options.key, 0);

        if(!params.WORDS) params.WORDS = this.getWords(params, true);

        if(params.PASSWORD) params.PASSWORD = hash.encrypt(key,  params.PASSWORD);

        const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&');

        return this.post('TopUpInit', query);
    }
    /**
     * Method for Check last balance DOKU
     * @param {Object} params 
     */
    CheckLastBalance(params){
        if(Object.keys(params).length < 1) throw { responsemsg: "The argument menthod canot be empty" }

        if(!params.REQUESTDATETIME) params.REQUESTDATETIME = DateTime.local().toFormat('yyyyLLddHHmmss');

        if(!params.WORDS) params.WORDS = this.getWords(params, true);

        const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&');

        return this.post('CheckLastBalance', query);
    }

    /**
     * Transaction Step 1
     * Method for show all inquiry from DOKU
     * @param {Object} params 
     */
    Inquiry(params){
        if(Object.keys(params).length < 1) throw { responsemsg: "The argument menthod canot be empty" };

        if(!params.REQUESTDATETIME) params.REQUESTDATETIME = DateTime.local().toFormat('yyyyLLddHHmmss');

        const createWords = `${params.CHANNELCODE}${params.SESSIONID}${params.REQUESTDATETIME}${this.options.key}${params.BILLERID}${params.ACCOUNT_NUMBER}`;
        
        if(!params.WORDS) params.WORDS = crypto.createHash('sha1').update(createWords).digest('hex');

        if(!params.SYSTRACE) {
            params.SYSTRACE = randomstring.generate({
                length: 8,
                charset: 'numeric'
            });
        }

        const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&');
        return this.post('Inquiry', query).then(result => {
            result.systrace = params.SYSTRACE;
            return result || {};
        });
    }

    /**
     * Transaction Step 2
     * Method for do payment after get the inquiry
     * @param {Object} params 
     */
    Payment(params){
        if(Object.keys(params).length < 1) throw { responsemsg: "The argument menthod canot be empty" };

        if(!params.REQUESTDATETIME) params.REQUESTDATETIME = DateTime.local().toFormat('yyyyLLddHHmmss');
        const key = hash.AESKeyVerifier(this.options.key, 0);
        const createWords = `${params.CHANNELCODE}${params.SESSIONID}${params.REQUESTDATETIME}${this.options.key}${params.BILLERID}${params.ACCOUNT_NUMBER}`;
        
        if(!params.WORDS) params.WORDS = crypto.createHash('sha1').update(createWords).digest('hex');
        if(params.PASSWORD) {
           params.PASSWORD = hash.encrypt(key,  params.PASSWORD);
        }
        const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&');

        return this.post('Payment', query).then(result => {
            result.systrace = params.SYSTRACE;
            return result || {};
        });
    }

    /**
     * Method For check status payment after payment is success
     * @param {Object} params 
     */
    Advise(params){
        if(Object.keys(params).length < 1) throw { responsemsg: "The argument menthod canot be empty" }
        if(!params.REQUESTDATETIME) params.REQUESTDATETIME = DateTime.local().toFormat('yyyyLLddHHmmss');

        const key = hash.AESKeyVerifier(this.options.key, 0);
        const createWords = `${params.CHANNELCODE}${params.SESSIONID}${params.REQUESTDATETIME}${this.options.key}${params.BILLERID}${params.ACCOUNT_NUMBER}`;
        
        if(!params.WORDS) params.WORDS = crypto.createHash('sha1').update(createWords).digest('hex');

        if(params.PASSWORD) params.PASSWORD = hash.encrypt(key, params.PASSWORD);

        const query = Object.keys(params).map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`).join('&');
        return this.post('Advise', query);
    }

    /**
     * Request Method gateway
     * @param {String} url 
     * @param {String} params 
     */
    post(url, params) {
        if(!url) throw { responsemsg : "Invalid argument"}
        return fetch(this.options.url + url + '?' + params, {
            method: 'POST'
        })
        .then(response => response.json())
        .then(response => {
            if(response.responsecode !== '0000') throw response;
            return response;
        })
    }
}

module.exports = Doku;
