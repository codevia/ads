const {DateTime} = require('luxon');
const DOKU = require('./index');

const opts = {
    url         : 'http://103.10.129.109/DepositSystem-api/',
    key         : 'shared123',
    merchantid  : '6012'
}

const payment = new DOKU(opts);

/* Login 
const params = {
    "CHANNELCODE":"6012",
    "REQUESTDATETIME": DateTime.local().toFormat('yyyyLLddHHmmss'),
    "LOGINNAME":"AgentTest3",
    "PASSWORD": "simpel"
}

payment.login(params).then(res => console.log(res)).catch(err => console.log(err));


/** TOPUP 
const params = {
    "CHANNELCODE":"6012",
    "SESSIONID":"63D82A22905B98841014E250024B7DD7.node3",
    "REQUESTDATETIME": DateTime.local().toFormat('yyyyLLddHHmmss'),
    "REQUESTTYPE": 1,
    "AMOUNT":10000,
    "TOPUPCHANNEL":"PULSA",
    "SENDERBANKNAME":"BCA",
    "SENDERBANKACCOUNTNAME":"Iwan Gunawan",
    "PASSWORD": "simpel"
}

payment.TopUpInit(params).then(res => console.log(res)).catch(err => console.log(err));


const params = {
    "CHANNELCODE":"6012",
    "SESSIONID":"63D82A22905B98841014E250024B7DD7.node3",
    "REQUESTDATETIME": DateTime.local().toFormat('yyyyLLddHHmmss')
}

payment.CheckLastBalance(params).then(res => console.log(res)).catch(err => console.log(err));
*/
let start = new Date().getTime();
const inq = {
    "CHANNELCODE":"6012",
    "SESSIONID":"10FCE79D7C660118D28C5F8890A97403.node3",
    "REQUESTDATETIME": DateTime.local().toFormat('yyyyLLddHHmmss'),
    "BILLERID":"9900015",
    "ACCOUNT_NUMBER":"085623400000",
    "ADDITIONALDATA1":"ISI PULSA INDOSAT"
}

payment.Inquiry(inq).then(res => {
    const params = {
        "CHANNELCODE":"6012",
        "SESSIONID":"10FCE79D7C660118D28C5F8890A97403.node3",
        "REQUESTDATETIME": DateTime.local().toFormat('yyyyLLddHHmmss'),
        "BILLERID":"9900015",
        "ACCOUNT_NUMBER":"085780458394",
        "INQUIRYID": res.inquiryid,
        "AMOUNT": "90500.00",
        "BILL_ID":"1",
        "ADDITIONALDATA1": "PEMBAYARAN",
        "PASSWORD": "simpel"
    }
    return payment.Payment(params);
})
.then(res => {
    const arg = {
        "CHANNELCODE":"6012",
        "SESSIONID":"10FCE79D7C660118D28C5F8890A97403.node3",
        "REQUESTDATETIME": DateTime.local().toFormat('yyyyLLddHHmmss'),
        "BILLERID":"9900015",
        "ACCOUNT_NUMBER":"085780458394",
        "HOST_REFERENCE_NUMBER": res.inquiryid,
        "AMOUNT": "90500.00",
        "BILL_ID":"1",
        "ADDITIONALDATA1": "PEMBAYARAN"
    }
    return payment.Advise(arg);
})
//.then(res => {
//    var end = new Date().getTime();
//    var time = end - start;
//    console.log('Execution time: ' + time);
//    console.log(new Date(),res);
//})
.catch(err => console.log(err));
    var end = new Date().getTime();
    var time = end - start;
    console.log('Execution time: ' + time);
