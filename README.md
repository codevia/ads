# DOKU AGENT DEPOSIT SYSTEM (ADS)
### Module Integrated API DOKU

### This is not official DOKU

## About DOKU
Established in 2007, we are the pioneers in online payment and the first with a risk management system in Indonesia. From sales, payment, purchase, transfer and lend, everything is possible with DOKU. With more than 10 years of experience, supported by a dedicated team, certified internationally and supervised under Bank of Indonesia, we are ready to be your partner in assisting you with all your transactional needs. [More >>](https://www.doku.com/about-us)

## Installation
```
npm install @iwandede/ads --save or yarn add @iwandede/ads
```
## Pre-requisites
You must have a merchant account on DOKU, as you can only get your keys (Merchant ID and Shared Key) after registering.

## Usage module
Below is all example usage the module

### Initial
```
const DOKU = require('@iwandede/ads');
const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);
```
### Check Network
```
const DOKU = require('@iwandede/ads');
const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);

// Promise result
payment.NetworkManagement().then(response => {
    // response in json format
    console.log(response);
}).catch(err => {
    // response in json format
    console.error(response);
})
```

### Login
```
const DOKU = require('@iwandede/ads');
const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);
const data = {
    "CHANNELCODE": options.merchantid,
    "LOGINNAME"  : <username>,
    "PASSWORD"   : <password>
}
// Promise result
payment.login(data).then(response => {
    // response in json format
    console.log(response);
}).catch(err => {
    // response in json format
    console.error(response);
})
```

### Topup Init
```
const DOKU = require('@iwandede/ads');
const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);
const data = {
    "CHANNELCODE"       : options.merchantid,
    "SESSIONID"         : <sessionid> // Session for each success login
    "REQUESTTYPE"       : 1, //Agent will receive email top up deposit confirmation
    "AMOUNT"            : 10543, //Transfer amount. Last three digits are randomized,
    "TOPUPCHANNEL"      : "PULSA",
    "SENDERBANKNAME"    : "BANK NAME",
    "SENDERBANKACCOUNTNAME": "BANK ACCOUNT NAME",
    "PASSWORD"          : "YOUR PASSWORD"
}

// Promise result
payment.TopUpInit(data).then(response => console.log(response))
.catch(err => console.error(response))
```

### Check Last Balance
```
const DOKU = require('@iwandede/ads');
const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);
const data = {
    "CHANNELCODE": options.merchantid,
    "SESSIONID"  : "SESSION FROM LOGIN METHOD",
}
// Promise result
payment.CheckLastBalance(data)
.then(response => console.log(response)).catch(err => console.error(err))
```

### Inquiry
```
const DOKU = require('@iwandede/ads');
const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);

const data = {
    "CHANNELCODE" : options.merchantid,
    "SESSIONID"   : "<Session for each success login>",
    "BILLERID" .  : "<BILLERID from list documentation>",
    "ACCOUNT_NUMBER": "<Account Number or Phone number>",
    "ADDITIONALDATA1": "Additional Information",
    "ADDITIONALDATA2": "Additional Information 2",
    "ADDITIONALDATA3": "Additional Information 3"
}
// Promise result
payment.Inquiry(data)
.then(response => console.log(response)).catch(err => console.error(err))
```

### Payment
```
const DOKU = require('@iwandede/ads');

const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);

const data = {
    "CHANNELCODE"    : options.merchantid,
    "SESSIONID"      : "<Session for each success login>",
    "BILLERID"       : "<BILLERID from list documentation>",
    "ACCOUNT_NUMBER" : "<Account Number or Phone number>",
    "INQUIRYID"      : "<Inquiry ID>",
    "AMOUNT"         : "90500.00",
    "BILL_ID" .      : "1",
    "ADDITIONALDATA1": "Additional Information",
    "ADDITIONALDATA2": "Additional Information 2",
    "ADDITIONALDATA3": "Additional Information 3"
}
// Promise result
payment.Payment(data)
.then(response => console.log(response)).catch(err => console.error(err))
```

### Advise
```
const DOKU = require('@iwandede/ads');

const options = {
    url         : <URL GIVEN BY DOKU>, //String
    key         : <KEY GIVEN BY DOKU>, //String
    merchantid  : <MERCHANTID GIVEN BY DOKU> //Srting
}
const payment = new DOKU(options);

const data = {
    "CHANNELCODE"    : options.merchantid,
    "SESSIONID"      : "<Session for each success login>",
    "BILLERID"       : "<BILLERID from list documentation>",
    "ACCOUNT_NUMBER" : "<Account Number or Phone number>",
    "HOST_REFERENCE_NUMBER"      : "<Inquiry ID>",
    "AMOUNT"         : "90500.00",
    "BILL_ID" .      : "1",
    "ADDITIONALDATA1": "Additional Information",
    "ADDITIONALDATA2": "Additional Information 2",
    "ADDITIONALDATA3": "Additional Information 3"
}
// Promise result
payment.Advise(data)
.then(response => console.log(response)).catch(err => console.error(err))
```

Documentation of integration can be seen or contact DOKU [Contact](https://www.doku.com/contact-us?lang=en)